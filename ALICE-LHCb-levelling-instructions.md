# ALICE and LHCb levelling

ALICE separation levelling is in the horizotal, LHCb separation levelling in the vertical plane.

* beta-star in ALICE 10 m, beam size is 54 um for emittance = 2 um.
* beta-star in LHCb 3 m, beam size is 30 um for emittance = 2 um.

When going into collisions the strategy is to stop the beams before they arive head-on, i.e. on the rising luminosiy curve.

For ALICE the separation knob is POSITIVE (B1 is on the + side, i.e outside wrt B2 which is inside), the knob is reduced until some luminosity is observed. Typically the separation is set to reach an initial luminosity between 0.1 and 1 (PAGE1 units). To increase the initial luminosity apply a NEGATIVE trim. A good step size is ~0.01 knob units (= mm) when the target is not too far.

For LHCb the separation knob is NEGATIVE (B1 is on the - side, i.e. below B2), the knob is increased towards 0 until some luminosity is observed. Typically the separation is set to reach an initial luminosity between 20 and 80 (PAGE1 units). To increase the initial luminosity apply a POSITIVE trim. A good step size is ~0.004 knob units (= mm) when the target is not too far.

When the targets are systematically to high/low from one fill to the next, consider adjusting this initial separation.

Note that the adjustement should be done consistently with the SEPARATION knobs. The lumi scan knobs in the levelling plane are set to 0 at the start of collisions.

![](images/Collisions/IR28-CollBP-Sep.gif)