# Nominal cycle instructions

## Injection settings

Unless one works ONLY with probes, the probe bunch intensity should not exceed 1E10 p (interlock BPM dumps when cleaning out etc).

* Tunes: **(0.275/0.293)**
* Chromaticity : **+15**
* Octupoles : **-4.2 (during filling) / -3.5 (before ramp)**

Filling should be performed with injection gap cleaning switched disabled (injection sequencer checkbox) because it seems to cause some light emittance blowup. As long as there are no RF issues in SPS or LHC this should not be a problem. In case injection losses are high, try enabling the injection gap cleaning and evaluate if it helps. The ADT gain for good injection cleaning is 0.3.

Filling schemes of intensity ramp up:

* Single_3b_2_2_2.csv
* Multi_12b_8_8_8_4bpi_3inj_2500ns.csv
* 25ns_75b_62_32_62_12bpi_9inj.csv
* 25ns_339b_326_206_266_12bpi_31inj.csv
* 25ns_315b_302_237_240_48bpi_11inj.csv
* 25ns_603b_590_524_542_48bpi_17inj.csv
* 25ns_603b_590_526_547_96bpi_13inj.csv
* 25ns_987b_974_876_912_96bpi_17inj.csv
* 25ns_987b_974_878_917_144bpi_13inj.csv
* 25ns_603b_590_524_542_48bpi_17inj.csv
* 25ns_1227b_1214_1054_1102_144bpi_14inj.csv
* 25ns_1551b_1538_1404_1467_144bpi_16inj.csv
* 25ns_1887b_1874_1694_1772_144bpi_19inj.csv
* 25ns_2175b_2162_1874_1964_144bpi_19inj.csv
* 25ns_2319b_2306_1964_2060_144bpi_22inj.csv
* 25ns_2460b_2448_2052_2154_144bpi_19injv2.csv

## Ramp

## Qchange

Since TS1 the new target tunes are 0.003 lower along the diagonal than the standard tunes : **(0.305/0.315)** (since MD2, was changed to 0.307/0.317 after TS1)
intead of the usual default of **(0.31/0.32)**. The QFB targets and the Q fitter windows were adapted to the new values. Those tunes are kept constant along the squeeze, and the change is taken out during the collision BP to end up at the optimized tunes for collisions.

## Squeeze

If the tune signal is poor and the QFB switches off, leave it off. The feedforward should be OK to keep the tune under control.

The octupole are lowered from -2.4 to -1.6 (knob) in the second half of the squeeze

## Adjust

Q' is reduced from **15 to 7** (-8 trim in the functions)
The tunes are moved from closer to the diagnonal around 0.313/0.318.

## Collisions

Optimize all, switch on levelling in ALICE & LHCb as soon as the optimization is finished. If the luminosity of LHCb and ALICE is consistently very low / very high, you can change the target of the IP_SEPARATION for IP2 and IP8. See instructions below.

Then declare stable beams.
* Watch out for the SIS orbit (new check task in preparation),
* Prepare OFB,
* RPs in,
* Switch on the abort gap cleaning for a few minutes to clean the gap (amplitude should be 0.22). The distribution should be ~uniform and the population should be 1-2E8 p.
* Emittance scan IP5. Emittance scans in IP1 only if ATLAS calls and requests one. Switch off OFB before scan. Don't forget to switch it back on after scan is finished.

As soon as the emittance scan is finished, reoptimize IP1 & IP5 and switch on continuous crossing angle levelling.

During the **first two hours it is very important to re-optimize IR1 and IR5** regularly because the corrections have a systematic drift - see figures below

Beta* levelling:
* Before beta* levelling update the OFB reference orbit from YASP. This is not option, but a key ingredient of a smooth levelling step for ALICE and LHCb. Use OFB gain (scaling factor) 3 (default value).
* Apply the 27cm beta* levelling with the crossing angle levelling is finished, which should correspond to ~9-10 hours of SB.
* Apply the 25cm beta* levelling around 1 hour after the 27 cm step. 

The **longitudinal blowup** should be applied when the bunch length reaches 0.95 ns for **LHCb polarity = +**

## Detailled information and supporting plots

# IP separation at start of SB for ALICE and LHCb

If the initial luminosity of ALICE or LHCb are consistently too low or too high, you can adjust the target separation in the collision BP **PHYSICS-6.5TeV-30cm-120s-2018_V1** with parameters LHCBEAM/IP2-SEP-H-MM and LHCBEAM/IP8-SEP-V-MM.
* ALICE: to lower (increase) the target lumi increase (reduce) the separation value of LHCBEAM/IP2-SEP-H-MM. We are coming in from the outside (B1).
* LHCb : to lower (increase) the target lumi reduce (increase) the separation value of LHCBEAM/IP8-SEP-V-MM. We are coming in from below (B1).
* The steps should be small, around 0.1 mm for ALICE, 0.05 mm for LHCb.

![](images/ALICE-LHCB-sep.gif)

# IP separation drifts ATLAS & CMS

Systematic separation shift (trim wrt first optimization in a fill) for IP1 and IP5

![](images/sep_ip1_2018.PNG)

![](images/sep_ip5_2018.PNG)

# Abort gap cleaning

## window settings

The amplitude should be set to 0.22 / medium.

The abort cleaining window settings are in BP **DISCRETE_LHCRING_ADT_FLATTOP**, SYSTEM ABORTGAP_CLEANER, Parameter type ALLAbortGapClean/WindowFunction. 
Drive the settings from LSA App Suite for the vertical planes in case you have doubts in the window function.

Example of a well cleaned abort gap

![](images/Abort-gap-cleaning.SB.png)


