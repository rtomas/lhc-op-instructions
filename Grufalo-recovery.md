# Grufalo recovery

After a Grufalo dump switch to a 900 bunch filling scheme to recover **25ns_987b_974_878_917_144bpi_13inj**.

This filling scheme has INDIVs that can latch the IQC injection oscillations if the settings of TI2/8 are not up to date. 
Before filling ask the SPS crew to copy the 25 ns cycel settings to the INDIV cycle.
If despite the copy the INDIVs latch the IQC, then temporarily mask the interlock in SIS (please do not forget to unmask).

Ramp and collide the 900b fill, observe the UFO activity and the loss spikes (from TIMBER). If the spike and UFO activity calms down and things are quiet for 2-3 hours, dump the 900b fill and switch back to the nominal.

![](images/20180531_170906.jpg)