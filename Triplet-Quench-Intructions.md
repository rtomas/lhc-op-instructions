# Triplet quench recovery

A triplet quench may result in a temporary or permanent change of the magnet positions which impacts the orbit and the collision overlap in SB.

Following a quench look at the WPS display that is normally opened below the orbit and tune FB real-time trims.

An example of a quench (ITR1 03.06.2018) and the subsequent position change as monitored by the Wire Positioning System (WPS) as shown in the figures below for the radial / x and height / y. In this case the WPS indicates that there seems to be no permanent position change.

If the position change is large, over 50 microns, one can expect a large impact on the beam overlap at ATLAS and CMS.  

![](images/WPS-radial-ITR1-quench.png)

![](images/WPS-height-ITR1-quench.png)
