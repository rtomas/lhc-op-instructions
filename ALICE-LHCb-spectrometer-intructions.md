# ALICE and LHCb spectrometers

For more details on the spectrometers see 

https://lhc-beam-operation-committee.web.cern.ch/lhc-beam-operation-committee/documents/Xing/Spectrometers-help.ppt

## LHCb spectrometer

The LHCb spectrometer and its compensator are located between Q1.L8 and Q1.R8. The bump is in the horizontal plane. The bump is totally invisible (no BPM). The spectrometer can can flipped sign or switched off tranparently. The only impact could be on the luminosity levelling that may have to be readusted slightly.

* When the spectrometer polarity is + the spectrometer bump adds to the external crossing angle (-170 urad at injection),
* When the spectrometer polarity is - the spectrometer bump compensates the external crossing angle (-170 urad at injection). 

## ALICE spectrometer

The ALICE spectrometer and its compensator are located between Q1.L2 and Q1.R2. The bump is in the vertical plane. The bump is totally invisible (no BPM). 

The situation in ALICE is more complicated than in LHCb due to the solenoid. At injection the solenoid couples the crossing bump ito the other plane. As a consequence flipping the polarity (usually solenoid and spectrometer are flipped together) there is a residual orbit error that has its source between the Q1s where there is no COD used by the orbit FB. Flipping the bump leaves residuals with amplitudes of up to ~0.5 mm at injection, typically in the form of a bump in the ALICE triplet region. At FT the error disappears due to the 1/Energy scaling of the perturbation.

* When the spectrometer polarity is - the spectrometer bump adds to the external crossing angle (200 urad at injection),
* When the spectrometer polarity is + the spectrometer bump compensates the external crossing angle (200 urad at injection). 

To note that flipping the polarity does not affect operation if one excludes the orbit bump at injection and may also be done at any time.

### 2018 ALICE setup

The 2018 reference orbit is setup for the - polarity of ALICE. A perturbation therefore appears when the polarity is flipped (eg after TS1 for vdm), below the B1 residual orbit with respect to the feedback reference. Because the MCBX are not used by the OFB, it cannot correct this perturbation.

![](images/Spectrometers/ALICE-PolPlus-OrbitB1.png)

A special knob was build to correct this perturbation locally around ALICE. The knobs for the two planes are **LHCBEAM/ALICE-INJ-POL-PLUS-V-2018** and **LHCBEAM/ALICE-INJ-POL-PLUS-H-2018** in system **ORBIT-BUMPS-OP**. For the + polarity the knobs should be set to 1 at injection, 0 at FT. A function that can be found in the trim history for the ramp is shown below. 

* To go to - polarity : trim the knobs to 0 at injection **and** in the ramp.
* To go to + polarity : trim the knobs to 1 at injection **and** reload the reference function for the ramp.

It is essential to trim both injection and ramp (use history). If only injection is trimmed then the sequencer incorporates the orbit at K-LEVEL and generates a bit of a mess in the ramp. It is not possible to define generic incoporation ranges for the various orbit knobs (Xing, Sep, Lumi, 162, ULO, etc) because each one follows a special logic which can itself change with the configuration.

![](images/Spectrometers/ALICE-PolPlus-knob.gif)

