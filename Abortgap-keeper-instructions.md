# Abortgap keeper update

Detailled instructions on the update of the abort gap can be found on the OP WEB pages:

https://be-op-lhc.web.cern.ch/content/agk-update-procedure

## Recovery from 90m run - July 2018

To switch back to the nominal abort gap keeper for 144b BCMS (filling scheme with 2556 bunches) revert the SIS reference using the MCS role MCS-LHC-SIS-EXPERT:

* In trim look for parameter LHC.SIS.AGK/AbortGapSettings, property SisAbortGapKeeper/AbortGapSettings, system LHC-SIS-REF of the non multiplexed BP _NON_MULTIPLEXED_LHC.
* Revert in the history to the settings: **02-05-2018 18:29:48, BCMS 3 batches of 48 spaced by 250 ns**.

Once the ABT colleagues will have reverted their settings on AKG and MKI, the SIS interlocks should all become TRUE again.

Follow the procedure to check that the AGK blocks the appropriate range of buckets without blocking the last legel bucket and the first bucket.