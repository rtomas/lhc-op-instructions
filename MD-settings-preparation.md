# Settings preparation for a standard MD

Steps to clone the settings / hypercycle for a standard MD using a clone of normal settings:

* Clone the hypercyle from HYPERC_NAME to HYPERC_NAME_MDx
* Clone all the PC function BPs (--> category MD) and append _MDx to all BP names.
* Insert the new MD BPs into HYPERC_NAME_MDx.
* For all actual BP **except INJECTION** generate the actual BPs for the new MD function BPs. Typically FT, end of qchange, end of squeeze, physics.
* Clone the INJECTION BP of the nomal cycle. **Expert action in the DB: rename the INJECTION clone and connect it to the clone of the ramp**.
* For the physics BP, generate the MCS settings for the lumi server parameters (MC role MCS-LHC-LUMI) by generating the actual BP settings using that MCS role.

