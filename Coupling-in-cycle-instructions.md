# Parasitic coupling measurements in the cycle

The ADT coupling measurement makes it possible to measure coupling parasitically uring regular operation.

The measurements can be made for exmaple on the fly during ramp and squeeze. Beam-beam introduces uncerntainties at the level of 0.001-0.002, values of C- of that order can be considered to be good with many bunches. Larger coupling values should be confirmed if possible by mesuring for at least 2 cycles. In case of larger coupling, apply 50% of the correction and then re-measure.

## Bunch selection

Ideally select a bunch that has no head-on in IP1 and IP5, and a few LR encounters as possible. In general good candidates are found among the first 12b train.

Good candidates for standard filling schemes:

* 25ns_2556b_2544_2215_2332_144bpi_20injV3 : use bunch **30** for beam1 (last 12b), bunch **7** for beam2 (first 12b).

## Amplitude

The recommended amplitude is **0.15**. Start with an amplitude of 0.1 to check any losses, then increase until the analysis is happy. With amplitudes of 0.2 it is possible to trigger losses at the warning level on the IR7 BLMs (this depends on beam conditions and machine phase).

## Tune

Pay attention to the tune - a standard trap is to use the injection tunes during the squeeze and with collisions. 
* For the squeeze tunes are now 0.307/0.317 (note that you have to type the numbers in !).
* For collisions use values like 0.313/0.317.