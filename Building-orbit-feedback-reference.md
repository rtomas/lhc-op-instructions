# How to build manually an orbit FB reference with YASP

Various YASP features can be used in combination to build powerful orbit feedback (OFB) reference on the fly for commissioning, MDs etc.

The procedure is intended to be used to create a new reference for manual or automatic (OFB) corrections of the orbit when a bump is inserted.

**It is recommended to open a separated YASP instance for the excercise, only used to repetetively create references. The orbit displayed is the one that gets saved or sent to BFSU...ALWAYS make sure that YASP is not acquiring!**

## INITIAL ORBIT

* Acquire the orbit
* Stop acquisition!
* Make sure you display the bare orbit (untick "Difference" and "FB reference")

![](images/YASP-control.png)

* SAVE the initial orbit in he MD catalog as starting points

![](images/MD-catalog.png)


## BUILD THE REFERENCE

* Go to menu Optics & Model -> LSA knob importer
* Choose the LSA parameter system

![](images/Knob-extract.png)

* Select the bump. You can select more than one, but beware that the same scale factor (next bullet) will be applied to all knobs.
* Type IN the Knob scale factor (NB: this is the value of the knob you want to trim i.e. in the case of reducing a Xing angle it whould be -150)
* Click on "Add to Orbit"
* The bump will be added to the orbit

![](images/Orbit-bump.png)

* Add as many bumps as you want (it's recommended to iteratively save the orbit in the MD catalog to have safe points to go back to, in case of problems or mistakes)

The REFERENCE can now be used to correct either manually, or... **Click on "Send active to BFSU"** from the FB Reference catalog and use the FB to correct.

![](images/active-BFSU.png)
