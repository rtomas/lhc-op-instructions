# High beta 

## Configuration

The hypercycle for highbeta is **6.5TeV_2018_HighBeta_90m**.

The sequence is **HIGHBETA TO 90M 2018** - it is in the MD tab of the sequencer QLP !

The cycle is composed of:
* A ramp-desqueeze to 67 m beta* (in the V plane, 45 m in H).
* A Qchange to the default target tunes of **(0.31/0.32)**.
* A first desqueeze to 75m (approx 30 minutes long).
* A second desqueeze to 90 m.
* Collision BP for IR1 and IR5 only. ALICE and LHCB remain separated.

The ramp up scenario with 100 ns and possiby 50 ns (tbc by tests as crossing angle very small, could be an issue with LRBB):

* 100 ns: 72 b -  300 b - 738 b, with trains of 72 b for the 300 b and 738 b fill. **For 100 ns beams use BUNCH_1 calibration**.
* 50 ns: 300 b - 700 b - 1452, with trains of 144 b for 700 b and 1452 b fill.

Filling schemes for 100 ns:
* 100ns_86b_84_0_0_18bpi_7inj
* 100ns_302b_300_0_0_72bpi_7inj
* 100ns_734b_732_0_0_72bpi_13inj 

Filling schemes for 50 ns:
* 50ns_302b_300_70_63_72bpi_7inj
* 50ns_734b_732_284_398_144bpi_11inj
* 50ns_1452b_1450_21_1128_144bpi_11inj

The bunh intensity should be around 1E11 ppb, in terms of emittance we take what the injectors gives us, most likely 1-1.5 um.

## Settings

The beam processes for this hypercycle are (currently) in category MD.

For IR2 and IR8 the optics does not change at all. Only the crossing and separation bumps are scaled dring the ramp to the standard end of ramp values of the nominal cycle.

The crossing angles in IR1 and IR5 start at 170 urad and are reduced to **60 urad** during the ramp. This crossing angle value is kept into physics. The abosulte maximum that may be possible is 65 urad due to corrector strength limitations, but would leave no margin for orbit FB etc.

## Preparation
The AFP pots have been put in an UNDEFINED state (on pourpose). As a result the 2 tasks below will fail in the SET OUT THRESHOLDS FOR ROMAN POTS sequence:
* LOAD INNER POTS LIMITS FOR XRP (for the 4 H pots in IP1)
* LOAD OUT-OF-BEAM-THRESHOLDS FOR AFP ROMAN POTS

## Injection

The setup of the cycle in terms of Q' is as the nominal cycle: +15.

The injection optics and configuration of this cycle is identical to our standard injection.

When operating with 100 ns and 50 ns beams, beware to take the correct calibration setting for the BPMs, BUNCH_1 for 100 ns beams (was used in 2015).

## Ramp

The orbit is sometimes ugly in the IRs. No worry, this is due to the limited setup time.

## Collisions

There are **NO COLLISIONS in ALICE and LHCb** !

Because the crossing angle bump is eating up almost all corrector strength, it may be difficult to perform luminosity optimizations in the crossing plane for ATLAS and CMS. An exception may be thrown by the lumi server when trying to optimize in the crossing plane.

**EMITTANCE SCANS**
* On both IP1/IP5 at the beginning of each fill of intensity ramp-up and additionally after 12h in normal physics fills
* The OFB must be OFF as, due to large beta value, the mm displacement is big and the OFB would counteract the scan

ATLAS (wait 15 min after collisions)
* CROSSING = 1 sigma (limited by current in RCBYVS4.L1B1)
* SEPARATION = 3 sigma

CMS
* CROSSING = 3.5 sigma
* SEPARATION = 3.5 sigma

## 90m preparation and recovery from 90m

### Preparation for 90m

* Switch off ALICE and LHCb spectrometers and solenoids.
* Change the TCT energy limits in the discrete BP to the 90m values (requires a collimation MCS role – Belen / Daniele / Roderick / Matteo).

The **beta-star limits for the SMP Safe Stable Beams flags** must be updated as the limit between min beta-star and max beta-star muts not exceed 30 m! The 90m setting is therefore not compatible with low-beta. The parameter to change is in the NON_MULTIPLEXED BP, system SMP, CISX/SqueezingFactorLimit as shown below. The lower/upper limit factors for IR1 muts be set to 3800 and 6500 (unist in cm, corresponds to 38 / 65 m), the limits for IR5 must be changed to 7800 and 9900 (unit in cm, corresponds to 78 / 99 m). To trim select the **critical role MCS-SMP** (you should have it as op-shift). 

Incorrect limits do not lead to any interlock or beam dump, but prevent the experiments from taking data and the movable devices from moving towards the beam as the **stable beams flag** and **movable devices allowed flags** will not switch to TRUE when the mode is stable beams.

![](images/Highbeta/SMP-flags-Page1.gif)

![](images/Highbeta/BetaStar-SMP-trim.gif)

### Recovery from 90m

* Switch ALICE and LHCb back on with the desired polarity.
* Change TCT energy limits in the discrete BP to nominal low-beta values (requires a collimation MCS role – Belen / Daniele / Roderick / Matteo).
* Revert to the default SMP beta-star limit values for the SMP flags. Use the trim history to find the correct setting, there is an entry for low-beta+vdm.
* Revert the AGK settings - see intructions for AGK !

