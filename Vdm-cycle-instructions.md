# VDM cycle overview

Hypercycle name is **6.5TeV_2017_mediumBeta**.

The squence is **'SEQUENCE FOR VDM'** in the MD tab of the quick launch panel

The cycle is composed of a ramp-desqueeze to 19m (IR1,2,5) and 24m (IR8), a Qchange and collision BP:
* RAMP-DESQUEEZE-19m-2016_V1
* QCHANGE-6.5TeV-19-24m-2016_V1 with standard tunes **(0.31/0.32)**
* PHYSICS-6.5TeV-19-24m-2016_V1

This cycle is still based on the old non-ATS optics (2016 version)! The injection BP is very similar but not identical to the current standard ATS injection, for this reason MP validation must also be performed at injection.

For IR1 and IR5 the crossing angles are switched off during the collision BP.

Filling schemes:
* For IP2/8 scans : Multi_70b_58_20_22_4bpi_19inj
* For IP1/5 scans : 525ns_140b_124_32_23_16bpi_11inj
* Bunch intensities 8 +- 1 e10 intensity (requested by LHCb)
* Bunch emittances ~ 3 microns

More details may be found at the link: https://lpc.web.cern.ch/SpecialRunConfigurations_2018.htm

## Preparation of VDM

* Change polarity of ALICE spectrometer and solenoid to positive/positive.
* Retract BRANS of ALICE.
* Change TCT settings in the non multiplexed BP to VdM values (requires a collimation MCS role – Belen / Daniele / Roderick / Matteo).
* Call low level RF to check all cavities before filling (issue with Cav8B2).
* BI check of FBCT.

## Special instructions

For this cycle **Q' is +10** in all phases.

Octupoles at injection set to -2 (knob value) ! Since we operate wih single bunches, the octupoles can be kept lower, around -2, even at injection !

With this non-ATS optics be very careful with the DAT coupling tool, it may either not work, or give incorrect results due to the difference of 2 units on the integer tunes.

**For the VdM we use the RF without Full Detuning (skip in the sequence)**

Optimize all experiments in both planes, even ALICE and LHCb !

## Recovery from VDM

* Switch off ALICE and LHCb spectrometers/solnoid for the 90m run.
* Reinsert BRANS of ALICE