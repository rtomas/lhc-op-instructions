The high accuracy beta values in IR4, measured by the OMC via K-modulation, are needed to compute the correct beam emittance.

In view of adding the possiblity of using the measured optics in the WireScanners app for emittance calculation, this file documents how these values are added to LSA.


When in the WS app the selected Optics type is : MEASURED, the app will pick the correct beta values at the WS element from LSA.

To every 'OPTIC' in a beam process one could associate a 'MEAS OPTIC'.

Using Delphine's application (now available in the CCM Under LHC Control -> Beam Control) : Meas Optic Uploader

The instructions are shown in the app snapshot:

![](images/OpticsMeasToLSA.png)


The files to be used were uploded to this directories:
/user/slops/data/LHC_DATA/OP_DATA/MeasOpticsToLSA/B1(B2)/INJECTION(FLATTOP)(SQUEEZED_30cm)

A sample file contains the following fields:


-bash-4.1$ less H_fromKmod_IR4.out
@ Measured Via KMOD in IR4 -- Analyzed yb OMC -- created by gtrad 2018/06/07

           NAME                 S             COUNT              BETX           SYSBETX          STATBETX           ERRBETX     CORR_ALFABETA              ALFX           SYSALFX          STATALFX           ERRALFX           BETXMDL           ALFXMDL            MUXMDL     NCOMBINATIONS

              %s               %le               %le               %le               %le               %le               %le               %le               %le               %le               %le               %le               %le               %le               %le               %le

     "BWS.5R4.B1"               0.0                 1             193.5               0.0               0.0               2.7               0.0               0.0               0.0               0.0               0.0               0.0               0.0               0.0                 0

    "MBRS.5R4.B1"               0.0                 1             199.7               0.0               0.0               2.9               0.0               0.0               0.0               0.0               0.0               0.0               0.0               0.0                 0

     "MU.B5R4.B1"               0.0                 1             204.2               0.0               0.0               2.9               0.0               0.0               0.0               0.0               0.0               0.0               0.0               0.0                 0

The present files contain only measured optics needed by the beam size measurement devices (Udulators, D3 and WS).
In case needed one can load following the same procedure the OMC measured optics at the BPMs stored in the directories:

/user/slops/data/LHC_DATA/OP_DATA/Betabeat/2018-04-02/LHCB1/Results/Injection_NormalOffMom/.
The files used by upload application are getbetax_free.out, getbetay_free.out, getDx.out and getDy.out. 






