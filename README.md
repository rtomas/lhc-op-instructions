# OP Instructions

This repository contains (hopefully) up-to-date information and instructions for operation crews of the LHC.

## How to contribute

All the files have to have the ending .md ([markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)) . A short overview of the syntax can be found [here]()