# Procedure to recover a COD failure

In case of a COD failure with OFB ON, the OFB will try to correct the perturbation. But teh matrix it is using still has the failing corrector included, which means that by construction the correction will pump even more current into the COD. Since the COD is failing and no longer acting on the beam, the OFB is pushing the current up steadily. This is visible on the two figures below where RCBV.21L1.B1 failed. There is a local residual orbit structure and the RT trims are huge, in particular for the COD that is now OFF.

![](images/COD-failure/Failed-CODV-Orbit.gif)

![](images/COD-failure/Failed-CODV-RTtrims.gif)

Once you are in this situation **DO NOT SWITCH THE COD back ON**. Leave the COD off. The OFB keepts the large correction and would apply it as soon as the COD is back on ! Instead go to the Machine Specials -> Feebback -> Masks menu. Find the faulty COD and disable it as shown below. Then send the status to the OFB (left side of panel, button "Set COD Status")

![](images/COD-failure/Failed-CODV-COD-status.gif)

Once the status is disabled, recalculate the response matrix from the YASP menu (or sequencer) and send it to the OFB. To send it to the OFB in manul mode, one must first check the update mode of the optics: it is indidate in the selection box (rightmost item ext to the bottons in the fiure below), in this example it is in the default where the update is by timing event. To make a manual update, set to the mode to **Manual Update** in case it is update by timing, send the optics, then switch back to timing event update mode.

Then switch the OFB back on and the situation will stabilize (but of course there will be remaining errors). To note that when this happens in stable beams (and if you are lucky enough that the beam is not gone through SIS interlocks), the overall perturbation maight be rather large due to the ow number of eigenvalues (40). In that case it is better to increase the number of eigenvalues to ~200-300), let the OFB apply a correction (1-2 minutes) before reverting to a low number of eigenvalues (back to the default of 40). Note that durin gthis process there may be a strong impact on the luminosities due to the large number of eigenvalues...

![](images/COD-failure/Failed-CODV-matrix2.gif)